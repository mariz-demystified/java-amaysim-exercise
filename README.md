# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is intended only for the Amaysim exercise.
* 0.0.1

### How do I get set up? ###

* Download the java-amaysim exercise zip file in this link: https://bitbucket.org/mariz-demystified/java-amaysim-exercise/downloads/?tab=branches.
* Extract and copy the Shopping Card Folder and subfolders into a specific directory of your choice.
* Make sure you have Java installed in your local machine. Open your command prompt and key in "java -version".
* Compile the source code. Key in `javac *.java` in the command prompt under the exercise folder ../ShoppingCart/src/main/java/com/amaysim/exercise.
* You should see now see compiled java classes.
* Run the test file. "java com.amaysim.exercise.ShoppingCartManager" under the root folder ../ShoppingCart/src/main/java
* Output snapshot is included inside ShoppingCart folder - Output.png
