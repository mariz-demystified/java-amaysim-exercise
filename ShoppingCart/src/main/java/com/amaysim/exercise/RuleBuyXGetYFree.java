package com.amaysim.exercise;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;

public class RuleBuyXGetYFree extends PricingRule {

	private Map<ShoppingCartItem, Integer> reqItems;
	private Map<ShoppingCartItem, Integer> freeItems;

	/**
	 * @param code
	 * @param name
	 * @param description
	 * @param startDate
	 * @param expiryDate
	 * @param isLimited
	 * @param isAutoApplied
	 * @param promoType
	 * @param reqItems
	 * @param freeItems
	 */
	public RuleBuyXGetYFree(String code, String name, String description, LocalDateTime startDate,
			LocalDateTime expiryDate, boolean isLimited, boolean isAutoApplied,
			com.amaysim.exercise.PricingRule.promoType promoType, Map<ShoppingCartItem, Integer> reqItems,
			Map<ShoppingCartItem, Integer> freeItems) {
		super(code, name, description, startDate, expiryDate, isLimited, isAutoApplied, promoType);
		this.reqItems = reqItems;
		this.freeItems = freeItems;
	}

	/**
	 * @return the reqItems
	 */
	public Map<ShoppingCartItem, Integer> getReqItems() {
		return reqItems;
	}

	/**
	 * @param reqItems
	 *            the reqItems to set
	 */
	public void setReqItems(Map<ShoppingCartItem, Integer> reqItems) {
		this.reqItems = reqItems;
	}

	/**
	 * @return the freeItems
	 */
	public Map<ShoppingCartItem, Integer> getFreeItems() {
		return freeItems;
	}

	/**
	 * @param freeItems
	 *            the freeItems to set
	 */
	public void setFreeItems(Map<ShoppingCartItem, Integer> freeItems) {
		this.freeItems = freeItems;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BuyXGetYFree [code=" + super.getCode() + ", name=" + super.getName() + ", description="
				+ super.getDescription() + ", startDate=" + super.getStartDate() + ", expiryDate="
				+ super.getExpiryDate() + ", isGreaterThanMinQty=" + super.isGreaterThanMinQty() + ", isActivated="
				+ super.isActivated(this.getExpiryDate()) + ", promoType=" + super.getPromoType() + "reqItems="
				+ reqItems + ", freeItems=" + freeItems + "]";
	}

	@Override
	boolean apply(ShoppingCart cart) {		
		if(this.isActivated(super.getExpiryDate())) {
			Integer freeItemsCount = super.reqItemsInCart(this.reqItems, cart.getCartItems());
			if(freeItemsCount > 0) {
				if(this.getPromoType().equals(com.amaysim.exercise.PricingRule.promoType.ITEM_BASED)) {
					for (ShoppingCartItem key : this.freeItems.keySet()) {
						key.setTotalPrice(BigDecimal.ZERO);
						key.setQty(freeItemsCount);
						cart.getCartItems().add(key);
					}
				} else {
					for (ShoppingCartItem key : this.freeItems.keySet()) {
						key.setTotalPrice(BigDecimal.ZERO);
						key.setQty(1);
						cart.getCartItems().add(key);
					}
				}
				return true;
			}
		}
		return false;
	}
}
