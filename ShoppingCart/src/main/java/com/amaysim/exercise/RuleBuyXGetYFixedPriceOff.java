package com.amaysim.exercise;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;

public class RuleBuyXGetYFixedPriceOff extends PricingRule {

	private Map<ShoppingCartItem, Integer> reqItems;
	private BigDecimal fixedPrice;

	/**
	 * @param code
	 * @param name
	 * @param description
	 * @param startDate
	 * @param expiryDate
	 * @param isLimited
	 * @param isAutoApplied
	 * @param promoType
	 * @param reqItems
	 * @param fixedPrice
	 */
	public RuleBuyXGetYFixedPriceOff(String code, String name, String description, LocalDateTime startDate,
			LocalDateTime expiryDate, boolean isLimited, boolean isAutoApplied,
			com.amaysim.exercise.PricingRule.promoType promoType, Map<ShoppingCartItem, Integer> reqItems, BigDecimal fixedPrice) {
		super(code, name, description, startDate, expiryDate, isLimited, isAutoApplied, promoType);
		this.reqItems = reqItems;
		this.fixedPrice = fixedPrice;
	}

	/**
	 * @return the reqItems
	 */
	public Map<ShoppingCartItem, Integer> getReqItems() {
		return reqItems;
	}

	/**
	 * @param reqItems
	 *            the reqItems to set
	 */
	public void setReqItems(Map<ShoppingCartItem, Integer> reqItems) {
		this.reqItems = reqItems;
	}

	/**
	 * @return the fixedPrice
	 */
	public BigDecimal getFixedPrice() {
		return fixedPrice;
	}

	/**
	 * @param fixedPrice
	 *            the fixedPrice to set
	 */
	public void setFixedPrice(BigDecimal fixedPrice) {
		this.fixedPrice = fixedPrice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RuleBuyXGetYFixedPriceOff [code=" + super.getCode() + ", name=" + super.getName() + ", description="
				+ super.getDescription() + ", startDate=" + super.getStartDate() + ", expiryDate="
				+ super.getExpiryDate() + ", isGreaterThanMinQty=" + super.isGreaterThanMinQty() + ", isActivated="
				+ super.isActivated(this.getExpiryDate()) + ", promoType=" + super.getPromoType() + "reqItems="
				+ reqItems + ", fixedPrice=" + fixedPrice + "]";
	}

	@Override
	boolean apply(ShoppingCart cart) {		
		boolean isApplied = false;
		if(this.isActivated(super.getExpiryDate())) {
			if(this.getPromoType().equals(com.amaysim.exercise.PricingRule.promoType.ITEM_BASED)) {
				for(ShoppingCartItem item : cart.getCartItems()){
					Integer reqQty = super.getRequiredItemQty(this.reqItems, item.getCode());				
					if(reqQty > 0 && item.getQty() >= reqQty) {
						if(this.isGreaterThanMinQty()) {
							if(item.getQty() > reqQty) {
								item.setTotalPrice(this.fixedPrice.multiply(new BigDecimal(item.getQty())));
								isApplied = true;
							}
						} else {
							Integer dQty = item.getQty()/reqQty;
							Integer ndQty = item.getQty() - (dQty*reqQty);
							item.setTotalPrice(this.fixedPrice.multiply(new BigDecimal(dQty))
									.add(item.getPrice().multiply(new BigDecimal(ndQty))));
							isApplied = true;
						}
					}
				}
			} else {
				for(ShoppingCartItem item : cart.getCartItems()){
					Integer reqQty = super.getRequiredItemQty(this.reqItems, item.getCode());				
					if(reqQty > 0 && item.getQty() >= reqQty) {
						if(this.isGreaterThanMinQty()) {
							if(item.getQty() > reqQty) {
								item.setTotalPrice(this.fixedPrice);
								isApplied = true;
							}
						} else {
							item.setTotalPrice(this.fixedPrice);
							isApplied = true;
						}
					}
				}
			}
		}
		return isApplied;
	}
}
