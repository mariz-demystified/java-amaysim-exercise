package com.amaysim.exercise;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class ShoppingCart {

	private List<ShoppingCartItem> cartItems;
	private Set<PricingRule> pricingRules;
	private String ruleCode;
	private BigDecimal grandTotal;
	private Integer totalQty;
	private RulesEngine rulesEngine;

	/**
	 * @param pricingRules
	 */
	public ShoppingCart(Set<PricingRule> pricingRules) {
		super();
		this.cartItems = new ArrayList<ShoppingCartItem>();
		this.pricingRules = pricingRules;
		this.ruleCode = "";
		this.grandTotal = BigDecimal.ZERO;
		this.totalQty = 0;
		this.rulesEngine = new RulesEngine();
	}

	/**
	 * @return the cartItems
	 */
	public List<ShoppingCartItem> getCartItems() {
		return cartItems;
	}

	/**
	 * @param cartItems
	 *            the cartItems to set
	 */
	public void setCartItems(List<ShoppingCartItem> cartItems) {
		this.cartItems = cartItems;
	}

	/**
	 * @return the pricingRules
	 */
	public Set<PricingRule> getPricingRules() {
		return pricingRules;
	}

	/**
	 * @param pricingRules
	 *            the pricingRules to set
	 */
	public void setPricingRules(Set<PricingRule> pricingRules) {
		this.pricingRules = pricingRules;
	}

	/**
	 * @return the ruleCode
	 */
	public String getRuleCode() {
		return ruleCode;
	}

	/**
	 * @param ruleCodes
	 *            the ruleCodes to set
	 */
	public void setRuleCode(String ruleCode) {
		this.ruleCode = ruleCode;
	}

	/**
	 * @return the grandTotal
	 */
	public BigDecimal getGrandTotal() {
		return grandTotal;
	}

	/**
	 * @param grandTotal
	 *            the grandTotal to set
	 */
	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

	/**
	 * @return the totalQty
	 */
	public Integer getTotalQty() {
		return totalQty;
	}

	/**
	 * @param totalQty
	 *            the totalQty to set
	 */
	public void setTotalQty(Integer totalQty) {
		this.totalQty = totalQty;
	}

	public void add(ShoppingCartItem item) {
		item.setQty(1);
		item.setTotalPrice(BigDecimal.ZERO);
		this.cartItems.add(item);
	}

	public void add(ShoppingCartItem item, String ruleCode) {
		item.setQty(1);
		item.setTotalPrice(BigDecimal.ZERO);
		this.cartItems.add(item);
		this.ruleCode = ruleCode;
	}

	public void remove(ShoppingCartItem item) {
		this.cartItems.remove(item);
	}

	// Calculate the grand total and total quantity applying the pricing rules
	// (auto-applied and manually-entered)
	public void total() {
		List<ShoppingCartItem> shoppingCartItems = new ArrayList<ShoppingCartItem>();
		for (ShoppingCartItem cartItem : this.cartItems) {
			if (!itemExists(shoppingCartItems, cartItem)) {
				ShoppingCartItem newCartItem = new ShoppingCartItem();
				newCartItem.setCode(cartItem.getCode());
				newCartItem.setName(cartItem.getName());
				newCartItem.setPrice(cartItem.getPrice());
				newCartItem.setQty(1);
				newCartItem.setTotalPrice(cartItem.getPrice());
				shoppingCartItems.add(newCartItem);
			} else {
				for (ShoppingCartItem item : shoppingCartItems) {
					if (item.getCode().equals(cartItem.getCode())) {
						item.setQty(item.getQty() + 1);
						item.setTotalPrice(item.getTotalPrice().add(item.getPrice()));
						break;
					}
				}
			}
		}
		this.cartItems = shoppingCartItems;

		// Apply auto-applied pricing rules
		this.rulesEngine.run(this, true, null);

		// Calculate grand total and total quantity after applying auto-applied
		// pricing rules
		for (ShoppingCartItem cartItem : this.cartItems) {
			this.grandTotal = this.grandTotal.add(cartItem.getTotalPrice());
			this.totalQty += cartItem.getQty();
		}
	}

	public void items() {
		System.out.println("Cart Items		Total");
		System.out.println("---------------------------------------");
		for (ShoppingCartItem cartItem : this.cartItems) {
			System.out.println(cartItem.getQty() + " x " + cartItem.getName() + "	"
					+ NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(cartItem.getTotalPrice()));
		}
		System.out.println("No. of Items: " + this.totalQty);
		System.out.println("SUBTOTAL: 		"
				+ NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(this.grandTotal));

		// Apply manually entered pricing rule (only one, the last rule entered)
		if (!this.ruleCode.isEmpty()) {
			if (this.rulesEngine.run(this, false, ruleCode)) {
				System.out.println(ruleCode + " applied.");
				System.out.println("TOTAL: 			"
						+ NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(this.grandTotal));
			}
		} else {
			System.out.println("TOTAL: 			"
					+ NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(this.grandTotal));
		}
	}

	private boolean itemExists(List<ShoppingCartItem> shoppingCartItems, ShoppingCartItem cartItem) {
		for (ShoppingCartItem item : shoppingCartItems) {
			if (item.getCode().equals(cartItem.getCode())) {
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ShoppingCart [items=" + cartItems.toString() + ", pricingRules=" + pricingRules.toString()
				+ ", ruleCode=" + ruleCode + ", grandTotal=" + grandTotal + ", totalQty=" + totalQty + "]";
	}
}
