package com.amaysim.exercise;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class RulePercentOff extends PricingRule {

	private BigDecimal percentOff;

	/**
	 * @param code
	 * @param name
	 * @param description
	 * @param startDate
	 * @param expiryDate
	 * @param isLimited
	 * @param isAutoApplied
	 * @param promoType
	 * @param percentOff
	 */
	public RulePercentOff(String code, String name, String description, LocalDateTime startDate,
			LocalDateTime expiryDate, boolean isLimited, boolean isAutoApplied,
			com.amaysim.exercise.PricingRule.promoType promoType, BigDecimal percentOff) {
		super(code, name, description, startDate, expiryDate, isLimited, isAutoApplied, promoType);
		this.percentOff = percentOff;
	}

	/**
	 * @return the percentOff
	 */
	public BigDecimal getPercentOff() {
		return percentOff;
	}

	/**
	 * @param percentOff
	 *            the percentOff to set
	 */
	public void setPercentOff(BigDecimal percentOff) {
		this.percentOff = percentOff;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PercentOff [code=" + super.getCode() + ", name=" + super.getName() + ", description="
				+ super.getDescription() + ", startDate=" + super.getStartDate() + ", expiryDate="
				+ super.getExpiryDate() + ", isGreaterThanMinQty=" + super.isGreaterThanMinQty() + ", isActivated="
				+ super.isActivated(this.getExpiryDate()) + ", promoType=" + super.getPromoType() + "percentOff="
				+ percentOff + "]";
	}

	@Override
	boolean apply(ShoppingCart cart) {
		if(this.isActivated(super.getExpiryDate())) {
			if(this.getPromoType().equals(com.amaysim.exercise.PricingRule.promoType.CART_BASED)) {
				cart.setGrandTotal(cart.getGrandTotal().subtract(cart.getGrandTotal().multiply(this.percentOff)));
				return true;
			}
		}
		return false;
	}
}
