package com.amaysim.exercise;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;

public abstract class PricingRule {

	private String code;
	private String name;
	private String description;
	private LocalDateTime startDate;
	private LocalDateTime expiryDate;
	private boolean isGreaterThanMinQty;
	private boolean isActivated;
	private boolean isAutoApplied;

	public enum promoType {
		CART_BASED, ITEM_BASED
	};

	private PricingRule.promoType promoType;

	abstract boolean apply(ShoppingCart cart);
	
	/**
	 * @param code
	 */
	public PricingRule(String code) {
		super();
		this.code = code;
	}

	/**
	 * @param code
	 * @param name
	 * @param description
	 * @param startDate
	 * @param expiryDate
	 * @param isGreaterThanMinQty
	 * @param isAutoApplied
	 * @param promoType
	 */
	public PricingRule(String code, String name, String description, LocalDateTime startDate, LocalDateTime expiryDate,
			boolean isGreaterThanMinQty, boolean isAutoApplied, com.amaysim.exercise.PricingRule.promoType promoType) {
		super();
		this.code = code;
		this.name = name;
		this.description = description;
		this.startDate = startDate;
		this.expiryDate = expiryDate;
		this.isGreaterThanMinQty = isGreaterThanMinQty;
		this.isAutoApplied = isAutoApplied;
		this.promoType = promoType;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the startDate
	 */
	public LocalDateTime getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the expiryDate
	 */
	public LocalDateTime getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @param expiryDate
	 *            the expiryDate to set
	 */
	public void setExpiryDate(LocalDateTime expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * @return the isLimited
	 */
	public boolean isGreaterThanMinQty() {
		return isGreaterThanMinQty;
	}

	/**
	 * @param isGreaterThanMinQty
	 *            the isGreaterThanMinQty to set
	 */
	public void setGreaterThanMinQty(boolean isGreaterThanMinQty) {
		this.isGreaterThanMinQty = isGreaterThanMinQty;
	}

	/**
	 * @return the isAutoApplied
	 */
	public boolean isAutoApplied() {
		return isAutoApplied;
	}

	/**
	 * @param isAutoApplied the isAutoApplied to set
	 */
	public void setAutoApplied(boolean isAutoApplied) {
		this.isAutoApplied = isAutoApplied;
	}

	/**
	 * @return the isActivated
	 */
	public boolean isActivated(LocalDateTime expiryDate) {
		return LocalDateTime.of(LocalDate.now(), LocalTime.now()).isBefore(expiryDate);
	}

	/**
	 * @param isActivated
	 *            the isActivated to set
	 */
	public void setActivated(boolean isActivated) {
		this.isActivated = isActivated;
	}

	/**
	 * @return the promoType
	 */
	public PricingRule.promoType getPromoType() {
		return promoType;
	}

	/**
	 * @param promoType
	 *            the promoType to set
	 */
	public void setPromoType(PricingRule.promoType promoType) {
		this.promoType = promoType;
	}
	
	public Integer getRequiredItemQty(Map<ShoppingCartItem, Integer> reqItems, String itemCode){		
		for (Item key : reqItems.keySet()) {
		    if(key.getCode().equals(itemCode)) {
		    	return reqItems.get(key);
		    }
		}
		return 0;
	}
	
	public Integer reqItemsInCart(Map<ShoppingCartItem, Integer> reqItems, List<ShoppingCartItem> cartItems){
		Integer retNoOfFreeItems = 0;	
		for (Item key : reqItems.keySet()) {
			Integer reqQty = reqItems.get(key);
			Integer noOfFreeItems = 0;	
			
			for(ShoppingCartItem item: cartItems) {
			    if(key.getCode().equals(item.getCode())
			    		&& item.getQty() >= reqQty) {
			    	noOfFreeItems = item.getQty()/reqQty;
			    	break;
			    }
			}
			if(noOfFreeItems > 0) {
				retNoOfFreeItems = noOfFreeItems;
			} else {
				return 0;
			}
		}
		return retNoOfFreeItems;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PricingRule [code=" + code + ", name=" + name + ", description=" + description + ", startDate="
				+ startDate + ", expiryDate=" + expiryDate + ", isGreaterThanMinQty=" + isGreaterThanMinQty + ", isActivated=" + isActivated
				+ ", promoType=" + promoType + "]";
	}
}
