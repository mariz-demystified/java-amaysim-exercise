package com.amaysim.exercise;

public class RulesEngine {
	
	boolean run(ShoppingCart cart, Boolean isAutoApplied, String promoCode) {
		boolean isApplied = false;
		for(PricingRule rule : cart.getPricingRules()) {
			if(rule.isAutoApplied() && isAutoApplied) {
				isApplied = rule.apply(cart);
			} else if (!rule.isAutoApplied() && !isAutoApplied 
					&& promoCode.equals(rule.getCode())){
				isApplied = rule.apply(cart);
			}
		}
		return isApplied;
	}
}
