package com.amaysim.exercise;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ShoppingCartManager {

	public static void main(String[] args) {

		//Add items in the catalogue
		ShoppingCartItem simUltSmall = new ShoppingCartItem("ult_small", "Unlimited 1GB", new BigDecimal(24.90));
		ShoppingCartItem simUltMedium = new ShoppingCartItem("ult_medium", "Unlimited 2GB", new BigDecimal(29.90));
		ShoppingCartItem simUltLarge = new ShoppingCartItem("ult_large", "Unlimited 5GB", new BigDecimal(44.90));
		ShoppingCartItem sim1Gb = new ShoppingCartItem("1gb", "1 GB Data-pack", new BigDecimal(9.90));
		
		//Add Special Offers and Promotions
		//A 3 for 2 deal on Unlimited 1GB Sims. So for example, 
		//if you buy 3 Unlimited 1GB Sims, you will pay the price of 2 only for the first month.
		Map<ShoppingCartItem, Integer> reqItems = new HashMap<ShoppingCartItem, Integer>();
		reqItems.put(simUltSmall, new Integer(3));
		RuleBuyXGetYFixedPriceOff ruleBuy1Ult1gfpo2 = new RuleBuyXGetYFixedPriceOff("b3ult1gfpo2", 
				"A 3 for 2 deal on Unlimited 1GB Sims. So for example, ", 
				"The Unlimited 5GB Sim will have a bulk discount applied; "
				+ "if you buy 3 Unlimited 1GB Sims, you will pay the price of 2 only for the first month.",
				LocalDateTime.of(2017, Month.MAY, 5, 0, 0, 0),
				LocalDateTime.of(2017, Month.JUNE, 5, 0, 0, 0),
				false, true, PricingRule.promoType.ITEM_BASED, 
				reqItems, simUltSmall.getPrice().multiply(new BigDecimal(2)));
		
		//The Unlimited 5GB Sim will have a bulk discount applied; 
		//whereby the price will drop to $39.90 each for the first month, if the customer buys more than 3.
		reqItems = new HashMap<ShoppingCartItem, Integer>();
		reqItems.put(simUltLarge, new Integer(3));
		RuleBuyXGetYFixedPriceOff ruleBuyMoreThan3Ult5gfpo = new RuleBuyXGetYFixedPriceOff("bm3ult5gfpo", 
				"Buy More than 3 Unlimited 5 Gb, Get $39.90 each", 
				"The Unlimited 5GB Sim will have a bulk discount applied; "
				+ "whereby the price will drop to $39.90 each for the first month, "
				+ "if the customer buys more than 3.", 
				LocalDateTime.of(2017, Month.MAY, 5, 0, 0, 0),
				LocalDateTime.of(2017, Month.JUNE, 5, 0, 0, 0), 
				true, true, PricingRule.promoType.ITEM_BASED, 
				reqItems, new BigDecimal(39.90));
		
		//We will bundle in a free 1 GB Data-pack free-of-charge with every Unlimited 2GB sold.
		reqItems = new HashMap<ShoppingCartItem, Integer>();
		reqItems.put(simUltMedium, new Integer(1));
		Map<ShoppingCartItem, Integer> freeItems = new HashMap<ShoppingCartItem, Integer>();
		freeItems.put(sim1Gb, new Integer(1));
		RuleBuyXGetYFree ruleBuy1Ult2Get1Ult1 = new RuleBuyXGetYFree("b1ult2g1ult1", 
				"Buy 1 Unlimited 2Gb Get Free 1Gb Data-pack.", 
				"Bundle in a free 1 GB Data-pack free-of-charge with every Unlimited 2GB sold.",
				LocalDateTime.of(2017, Month.MAY, 5, 0, 0, 0),
				LocalDateTime.of(2017, Month.JUNE, 5, 0, 0, 0),
				false, true, PricingRule.promoType.ITEM_BASED, 
				reqItems, freeItems);

		//Adding the promo code 'I<3AMAYSIM' will apply a 10% discount across the board.
		RulePercentOff ruleILoveAmaysim = new RulePercentOff("I<3AMAYSIM", 
				"10% off if the code is applied",
				"Promo code 'I<3AMAYSIM' will apply a 10% discount across the board.",
				LocalDateTime.of(2017, Month.MAY, 5, 0, 0, 0),
				LocalDateTime.of(2017, Month.JUNE, 5, 0, 0, 0),
				true, false, PricingRule.promoType.CART_BASED, new BigDecimal(0.10));
		
		
		//Add all rules to the rule set
		Set<PricingRule> pricingRules = new HashSet<PricingRule>();
		pricingRules.add(ruleBuy1Ult1gfpo2);
		pricingRules.add(ruleBuyMoreThan3Ult5gfpo);
		pricingRules.add(ruleBuy1Ult2Get1Ult1);
		pricingRules.add(ruleILoveAmaysim);
		
		//Test Scenario 1 - 3 x Unlimited 1 GB, 1 x Unlimited 5 GB
		System.out.println("---------------------------------------");
		System.out.println("Scenario 1");
		System.out.println("---------------------------------------");
		ShoppingCart cart1 = new ShoppingCart(pricingRules);
		cart1.add(simUltSmall);
		cart1.add(simUltSmall);
		cart1.add(simUltSmall);
		cart1.add(simUltLarge);
		cart1.total();
		cart1.items();
		
		//Test Scenario 2 - 2 x Unlimited 1 GB, 4 x Unlimited 5 GB
		System.out.println("---------------------------------------");
		System.out.println("Scenario 2");
		System.out.println("---------------------------------------");
		ShoppingCart cart2 = new ShoppingCart(pricingRules);
		cart2.add(simUltSmall);
		cart2.add(simUltSmall);
		cart2.add(simUltLarge);
		cart2.add(simUltLarge);
		cart2.add(simUltLarge);
		cart2.add(simUltLarge);
		cart2.total();
		cart2.items();
		
		//Test Scenario 3 - 1 x Unlimited 1 GB, 2 X Unlimited 2 GB
		System.out.println("---------------------------------------");
		System.out.println("Scenario 3");
		System.out.println("---------------------------------------");
		ShoppingCart cart3 = new ShoppingCart(pricingRules);
		cart3.add(simUltSmall);
		cart3.add(simUltMedium);
		cart3.add(simUltMedium);
		cart3.total();
		cart3.items();
		
		//Test Scenario 4 - 1 x Unlimited 1 GB, 1 x 1 GB Data-pack, 'I<3AMAYSIM' Promo Applied
		System.out.println("---------------------------------------");
		System.out.println("Scenario 4");
		System.out.println("---------------------------------------");
		ShoppingCart cart4 = new ShoppingCart(pricingRules);
		cart4.add(simUltSmall);
		cart4.add(sim1Gb, "I<3AMAYSIM");
		cart4.total();
		cart4.items();		
	}
}
