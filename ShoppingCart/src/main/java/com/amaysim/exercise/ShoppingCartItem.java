package com.amaysim.exercise;

import java.math.BigDecimal;

public class ShoppingCartItem extends Item {
	
	private Integer qty;
	private BigDecimal totalPrice;
		
	/**
	 * 
	 */
	public ShoppingCartItem() {
		super();
	}

	/**
	 * @param code
	 * @param name
	 * @param price
	 */
	public ShoppingCartItem(String code, String name, BigDecimal price) {
		super(code, name, price);
		this.qty = 1;
		this.totalPrice = price;
	}
	
	/**
	 * @return the qty
	 */
	public Integer getQty() {
		return qty;
	}
	
	/**
	 * @param qty the qty to set
	 */
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	
	/**
	 * @return the totalPrice
	 */
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	
	/**
	 * @param totalPrice the totalPrice to set
	 */
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ShoppingCartItem [code=" + super.getCode() + ", name=" + super.getName() + ", price=" + super.getPrice() + ", qty=" + qty + ", totalPrice=" + totalPrice + "]";
	}
}
