package com.amaysim.exercise;

import java.math.BigDecimal;

public class Item {
	
	private String code;
	private String name;
	private BigDecimal price;
			
	/**
	 * 
	 */
	public Item() {
		super();
	}

	/**
	 * @param code
	 * @param name
	 * @param price
	 */
	public Item(String code, String name, BigDecimal price) {
		super();
		this.code = code;
		this.name = name;
		this.price = price;
	}
	
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Item [code=" + code + ", name=" + name + ", price=" + price + "]";
	}
	
	
}
